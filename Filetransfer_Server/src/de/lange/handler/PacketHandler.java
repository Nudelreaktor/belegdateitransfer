package de.lange.handler;

import java.net.DatagramPacket;
import java.util.Map;

/**
 * 
 * @author Franz Lange 07.11.2015
 *
 */
public interface PacketHandler {
	
	public boolean valitatePacket(DatagramPacket datagramPacket);
	
	public Map<String, Object> getDataMap(DatagramPacket datagramPacket);
	public DatagramPacket getPacket(Map<String, Object> dataMap);
	
}
