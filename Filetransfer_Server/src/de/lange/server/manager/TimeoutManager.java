package de.lange.server.manager;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 
 * @author Franz Lange
 * 23.08.2015
 *
 */
public class TimeoutManager extends Thread {
	
	private volatile boolean interrupted;
	
	private Map<Short,Long> timeouts;
	
	public TimeoutManager(){
		this.interrupted = false;
		this.timeouts = new HashMap<Short,Long>();
	}
	
	/**
	 * Adds a session to the timeout
	 * @param sessionNumber
	 */
	public void addTimeout(short sessionNumber){
		if(!timeouts.containsKey(sessionNumber))
			timeouts.put(sessionNumber, new Date().getTime());
	}
	
	/**
	 * Updates a Sessions timeout
	 * @param sessionNumber
	 * @return true if the timeout was updated, false if the session doesn't exist
	 */
	public boolean updateTimeout(short sessionNumber){
		if(timeouts.containsKey(sessionNumber)){
			timeouts.put(sessionNumber, new Date().getTime());
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void run(){
		while(!interrupted){
			long time = new Date().getTime();
			Set<Short> keys = timeouts.keySet();
			for(Short key : keys){
				if(!timeouts.containsKey(key))
					continue;
				if(timeouts.get(key) <= (time - 2000)){
					timeouts.remove(key);
					System.out.println("[ INFO  ] Timeout: " + key);
				}
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {}
		}
	}
	
	@Override
	public void interrupt(){
		this.interrupted = true;
	}
	
	@Override
	public boolean isInterrupted(){
		return this.interrupted;
	}
}
