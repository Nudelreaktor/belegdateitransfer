package de.lange.server.manager;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.CRC32;

import de.lange.server.filewriter.FileWriter;
import de.lange.server.handler.ServerPacketHandler;
import de.lange.server.receiver.Receiver;
import de.lange.server.transmitter.Transmitter;

/**
 * 
 * @author Franz Lange 22.08.2015
 *
 */
public class ReceiveManager extends Thread {

	private volatile boolean interrupted;

	private Receiver receiver;
	private Transmitter transmitter;
	private Map<Short, FileWriter> fileWriters;
	private TimeoutManager timeoutManager;
	private ServerPacketHandler packetHandler;

	/**
	 * 
	 * @param port
	 * @throws SocketException
	 */
	public ReceiveManager(int port) throws SocketException {
		this.interrupted = false;
		this.receiver = new Receiver(port);
		this.receiver.start();
		this.transmitter = new Transmitter();
		this.transmitter.start();
		this.fileWriters = new HashMap<Short, FileWriter>();
		this.timeoutManager = new TimeoutManager();
		this.timeoutManager.start();
		this.packetHandler = new ServerPacketHandler();
	}

	/**
	 * 
	 * @param port
	 * @param looserate
	 * @throws SocketException
	 */
	public ReceiveManager(int port, int looserate) throws SocketException {
		this.interrupted = false;
		this.receiver = new Receiver(port, looserate);
		this.receiver.start();
		this.transmitter = new Transmitter();
		this.transmitter.start();
		this.fileWriters = new HashMap<Short, FileWriter>();
		this.timeoutManager = new TimeoutManager();
		this.timeoutManager.start();
		this.packetHandler = new ServerPacketHandler();
	}

	/**
	 * 
	 */
	@Override
	public void run() {
		while (!this.isInterrupted()) {
			DatagramPacket datagramPacket = this.receiver.getPacket();
			if (datagramPacket != null) {
				try {
					receivedPacket(datagramPacket);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		if (!fileWriters.isEmpty()) {
			for (Entry<Short, FileWriter> entry : fileWriters.entrySet()) {
					fileWriters.remove(entry.getKey());
			}
		}
		receiver.interrupt();
		try {
			receiver.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Deals with received DatagramPacket.
	 * 
	 * @param datagramPacket
	 *            deal with it!
	 * @throws IOException 
	 */
	private void receivedPacket(DatagramPacket datagramPacket) throws IOException {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		// deal with start packet
		if(packetHandler.validateStartPacket(datagramPacket)){
			dataMap = packetHandler.getStartDataMap(datagramPacket);
			timeoutManager.addTimeout((short)dataMap.get("sessionNumber"));
			fileWriters.put((short)dataMap.get("sessionNumber"), new FileWriter((String) dataMap.get("fileName"), (int) dataMap.get("fileSize")));
			
			DatagramPacket response = packetHandler.getPacket(dataMap);
			response.setPort(datagramPacket.getPort());
			response.setAddress(datagramPacket.getAddress());
			
			transmitter.sendPacket(response);
			
			System.out.println("[ INFO  ] New Client: " + dataMap.get("sessionNumber"));
		} else
		// protocol
		if(packetHandler.valitatePacket(datagramPacket)){
			dataMap = packetHandler.getDataMap(datagramPacket);
			
			if(!timeoutManager.updateTimeout((short)dataMap.get("sessionNumber")) 
					&& !fileWriters.containsKey((short)dataMap.get("sessionNumber")))
				return;
			
			fileWriters.get((short) dataMap.get("sessionNumber")).putBytes((byte[])dataMap.get("data"));
			
			if(dataMap.containsKey("checksum")){
				CRC32 crc = new CRC32();
				crc.update(fileWriters.get((short) dataMap.get("sessionNumber")).getBytes());
				if(((int)crc.getValue()) == (int)dataMap.get("checksum")){
					fileWriters.get((short) dataMap.get("sessionNumber")).write();
					fileWriters.remove((short) dataMap.get("sessionNumber"));
			
				} else {
					System.err.println("[ ERROR ] Wrong CRC32 checksum.\n\tCalculated: " + (int)crc.getValue() + "\n\tExpected: " + (int)dataMap.get("checksum"));
					return;
				}
			}
			
			DatagramPacket response = packetHandler.getPacket(dataMap);
			response.setPort(datagramPacket.getPort());
			response.setAddress(datagramPacket.getAddress());
			
			transmitter.sendPacket(response);
		}
	}

	/**
	 * Causes the ReceiveManager to stop working as early as possible.
	 */
	@Override
	public void interrupt() {
		this.interrupted = true;
	}

	/**
	 * Returns the state of the Interrupt Flag.
	 */
	@Override
	public boolean isInterrupted() {
		return this.interrupted;
	}
}
