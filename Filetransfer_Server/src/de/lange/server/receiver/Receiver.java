package de.lange.server.receiver;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * This class represents a receiver for receiving DatagramPackets.
 * 
 * The received DatagramPackets are stored in a packetQueue so it can be taken
 * at any Time asynchronous to the receive Time.
 * 
 * @author Franz Lange 22.08.2015
 */
public class Receiver extends Thread {

	private volatile boolean interrupted;

	private DatagramSocket receiverSocket;
	private volatile ConcurrentLinkedQueue<DatagramPacket> packetQueue;
	
	private int looserate;

	private Receiver(){}
	
	/**
	 * Constructs an new Receiver and binds in on a specified Port
	 * 
	 * @param port
	 *            the Port to use.
	 * @throws SocketException
	 *             if the socket could not be opened, or the socket could not
	 *             bind to the specified local port.
	 */
	public Receiver(int port) throws SocketException {
		this.interrupted = false;
		this.receiverSocket = new DatagramSocket(port);
		this.receiverSocket.setSoTimeout(1000);
		this.packetQueue = new ConcurrentLinkedQueue<DatagramPacket>();
		this.looserate = 0;
	}
	
	/**
	 * Constructs an new Receiver and binds in on a specified Port and sets a rate in percent were {@link DatagramPacket DatagramPackets} are not received.<br>
	 * Use only for debug purpose.
	 * 
	 * @param port
	 * @param looserate
	 * @throws SocketException
	 */
	public Receiver(int port, int looserate) throws SocketException {
		this(port);
		this.looserate = looserate;
	}

	/**
	 * runs the receiver. Receiver receive DatagrammPackets on specified Port
	 * and stores them in an Packet Queue.
	 * 
	 * packets have a maximum size of 2048 Bytes
	 */
	@Override
	public void run() {
		do {
			try {
				// receive Packet
				DatagramPacket datagramPacket = new DatagramPacket(new byte[2048], 2048);
				receiverSocket.receive(datagramPacket);
//				System.out.println("[ DEBUG ] Received Packet.");
				if(looserate <= new Random().nextInt(101))
				// put received packet in Queue
				packetQueue.offer(datagramPacket);
			} catch (SocketTimeoutException e) {
			} catch (IOException e) {
				e.printStackTrace();
			}
		} while (!this.isInterrupted());
	}

	/**
	 * sets an Interrupt Flag that causes the Receiver to stop at a safety
	 * moment.
	 */
	@Override
	public void interrupt() {
		this.interrupted = true;
		currentThread().interrupt();
	}

	/**
	 * checks whether the Interrupt Flag is set or not
	 * 
	 * @return the state of the Interrupt Flag
	 */
	@Override
	public boolean isInterrupted() {
		return interrupted;
	}

	/**
	 * Function removes the oldest received DatagrammPacket from the packet
	 * Queue and returns it.
	 * 
	 * @return the oldest received DatagrammPacket. null if their is no Packet
	 *         in Queue.
	 */
	public DatagramPacket getPacket() {
		return packetQueue.poll();
	}
}
