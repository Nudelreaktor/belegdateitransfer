package de.lange.server.filewriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 
 * @author Franz Lange 22.08.2015
 *
 */
public class FileWriter {
	private ByteBuffer byteBuffer;
	private String fileName;
	private int fileSize;
	
	private int bufferSize;

	public FileWriter(String fileName, int fileSize) throws FileNotFoundException {
		
		this.fileName = fileName;
		bufferSize = 0;
		
		this.fileSize = fileSize;
		
		byteBuffer = ByteBuffer.allocate(bufferSize);
	}

	/**
	 * buffers a byte-array
	 * 
	 * @param b
	 */
	public void putBytes(byte[] b) {
		ByteBuffer newBuffer = ByteBuffer.allocate(bufferSize + b.length);
		byteBuffer.flip();
		newBuffer.put(byteBuffer).put(b);
		byteBuffer = newBuffer;
		bufferSize += b.length;
	}

	/**
	 * buffers a String.
	 * 
	 * @param str
	 */
	public void putString(String str) {
		putBytes(str.getBytes());
	}
	
	/**
	 * 
	 * @return the buffered byte-array
	 */
	public byte[] getBytes(){
		byteBuffer.position(0);
		byte[] bufferedBytes = new byte[fileSize];
		byteBuffer.get(bufferedBytes,0,fileSize);
		
		return bufferedBytes;
	}

	/**
	 * Writes all buffered data in File given by fileName.
	 * 
	 * @throws IOException
	 */
	public void write() throws IOException {
		String[] parts = fileName.split("\\.");
		int i = 1;
		String baseFileName = parts[0];
		String newFileName = fileName;
		do {
			File file = new File(newFileName);
			if(file.exists()){
				parts[0] = baseFileName + "(" + i + ")";
				newFileName = "";
				for(String s : parts)
					newFileName += (s + ".");
				i++;
			} else
				break;
		} while(true);
		
		FileOutputStream fileOutputStream = new FileOutputStream(newFileName);
		byteBuffer.flip();
		fileOutputStream.write(byteBuffer.array());
		fileOutputStream.flush();
		fileOutputStream.close();
		System.out.println("[ INFO  ] File written: " + newFileName);
	}
}
