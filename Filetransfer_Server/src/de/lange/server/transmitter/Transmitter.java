package de.lange.server.transmitter;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class Transmitter extends Thread {
	private volatile boolean interrupted;
	
	private DatagramSocket serverSocket;
	
	public Transmitter() throws SocketException {
		interrupted = false;
		
		serverSocket = new DatagramSocket();
//		System.out.println("[ DEBUG ] transmitter Socket port: " + serverSocket.getLocalPort());
//		System.out.println("[ DEBUG ] transmitter Socket bining state: " + serverSocket.isBound());
		
	}
	
	@Override
	public void run(){
		while(!interrupted){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
		}
	}
	
	public void sendPacket(DatagramPacket datagramPacket) throws IOException{
		serverSocket.send(datagramPacket);
	}
	
	/**
	 * sets an Interrupt Flag that causes the Transmitter to stop at a safety
	 * moment.
	 */
	@Override
	public void interrupt() {
		this.interrupted = true;
		super.interrupt();
	}

	/**
	 * checks whether the Interrupt Flag is set or not
	 * 
	 * @return the state of the Interrupt Flag
	 */
	@Override
	public boolean isInterrupted() {
		return interrupted;
	}
}
