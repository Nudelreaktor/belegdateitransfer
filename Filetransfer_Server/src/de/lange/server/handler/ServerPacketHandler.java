package de.lange.server.handler;

import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.CRC32;

import de.lange.handler.PacketHandler;

/**
 * 
 * @author Franz Lange 07.11.2015
 *
 */
public class ServerPacketHandler implements PacketHandler {
	
	private Map<Short, Byte> lastClientPacketNumber;
	private Map<Short, Integer> currentPacketSizes;
	
	public ServerPacketHandler(){
		lastClientPacketNumber = new LinkedHashMap<Short, Byte>();
		currentPacketSizes = new LinkedHashMap<Short, Integer>();
	}
	
	public boolean validateStartPacket(DatagramPacket datagramPacket){
		try{
			Map<String, Object> dataMap = getStartDataMap(datagramPacket);
			
			// validate packet number
			if(!dataMap.containsKey("packetNumber"))
				throw new IllegalArgumentException("Invalid Datamap!");
			if((byte)dataMap.get("packetNumber") != 0)
				return false;
			
			// validate start
			if(!dataMap.containsKey("start"))
				throw new IllegalArgumentException("Invalid Datamap!");
			if(!"Start".equals(dataMap.get("start")))
				return false;
			
			
			
			// validate last packet number
			if(!dataMap.containsKey("sessionNumber"))
				throw new IllegalArgumentException("Invalid Datamap!");
			if(lastClientPacketNumber.containsKey((short)dataMap.get("sessionNumber")))
				if(lastClientPacketNumber.get((short)dataMap.get("sessionNumber")) != 0)
					return false;
			
			// validate checksum
			if(!dataMap.containsKey("checksum"))
				throw new IllegalArgumentException("Invalid Datamap!");
			CRC32 crc = new CRC32();
			crc.update(Arrays.copyOf(datagramPacket.getData(), datagramPacket.getLength()-4));
			int checksum = (int) crc.getValue();
			if(checksum != (int)dataMap.get("checksum"))
				return false;
			
			lastClientPacketNumber.put((short)dataMap.get("sessionNumber"),(byte)dataMap.get("packetNumber"));
			currentPacketSizes.put((short)dataMap.get("sessionNumber"),(int)dataMap.get("fileSize"));
			
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean valitatePacket(DatagramPacket datagramPacket) {
		try {
			Map<String, Object> dataMap = getDataMap(datagramPacket,true);
			
			// validate session number
			if(!dataMap.containsKey("sessionNumber"))
				throw new IllegalArgumentException("Invalid Datamap!");
			if(!lastClientPacketNumber.containsKey((short)dataMap.get("sessionNumber")))
				return false;
			// validate packet number
			if(!dataMap.containsKey("packetNumber"))
				throw new IllegalArgumentException("Invalid Datamap!");
			if((byte)lastClientPacketNumber.get((short)dataMap.get("sessionNumber")) == (byte)dataMap.get("packetNumber"))
				return false;
			
			lastClientPacketNumber.put((short)dataMap.get("sessionNumber"),(byte)dataMap.get("packetNumber"));
			
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public Map<String, Object> getStartDataMap(DatagramPacket datagramPacket) {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		
		ByteBuffer data = ByteBuffer.allocate(datagramPacket.getData().length);
		data.put(datagramPacket.getData());
		data.flip();
		
		short sessionNumber = data.getShort();
		dataMap.put("sessionNumber", sessionNumber);
		
		byte packetNumber = data.get();
		dataMap.put("packetNumber", packetNumber);

		byte[] start = new byte[5];
		data.get(start, 0, 5);
		dataMap.put("start", new String(start));
		
		int fileSize = data.getInt();
		dataMap.put("fileSize", fileSize);
		
		short fileNameLenght = data.getShort();
		dataMap.put("fileNameLength", fileNameLenght);
		
		byte[] fileName = new byte[fileNameLenght];
		data.get(fileName, 0, fileNameLenght);
		dataMap.put("fileName",new String(fileName));
		
		int checksum = data.getInt();
		dataMap.put("checksum", checksum);
		
		return dataMap;
	}

	@Override
	public Map<String, Object> getDataMap(DatagramPacket datagramPacket) {
		return getDataMap(datagramPacket, false);
	}
	
	private Map<String, Object> getDataMap(DatagramPacket datagramPacket, boolean validation){
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		ByteBuffer data = ByteBuffer.allocate(datagramPacket.getData().length);
		data.put(datagramPacket.getData());
		data.flip();
		
		short sessionNumber = data.getShort();
		dataMap.put("sessionNumber", sessionNumber);
		
		byte packetNumber = data.get();
		dataMap.put("packetNumber", packetNumber);
		
		byte[] actualData;
		
		if(datagramPacket.getLength()-3 < currentPacketSizes.get(sessionNumber)){
			actualData = new byte[datagramPacket.getLength()-3];
			data.get(actualData, 0, actualData.length);
			dataMap.put("data", actualData);
		} else {											// if packet is to big the checksum must be in
			actualData = new byte[datagramPacket.getLength()-7];
			data.get(actualData, 0, actualData.length);
			dataMap.put("data", actualData);
			
			int checksum = data.getInt();
			dataMap.put("checksum", checksum);
		}
		dataMap.put("size", actualData.length);
		
		if(!validation)
			currentPacketSizes.put(sessionNumber,currentPacketSizes.get(sessionNumber)-actualData.length);
		
		return dataMap;
	}

	@Override
	public DatagramPacket getPacket(Map<String, Object> dataMap) {
		ByteBuffer data = ByteBuffer.allocate(3);
		if(dataMap.containsKey("sessionNumber") && dataMap.containsKey("packetNumber")) {
			data.putShort((short)dataMap.get("sessionNumber"));
			data.put((byte)dataMap.get("packetNumber"));
		} else return null;
		
		data.flip();
		byte[] dataArray = new byte[3];
		data.get(dataArray,0, 3);
		
		return new DatagramPacket(dataArray, 3);
	}

	public void clean(short sessionNumber){
		if(lastClientPacketNumber.containsKey(sessionNumber))
			lastClientPacketNumber.remove(sessionNumber);
		if(currentPacketSizes.containsKey(sessionNumber))
			currentPacketSizes.remove(sessionNumber);
	}
	
}
