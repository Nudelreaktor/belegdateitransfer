package de.lange.server;

import java.io.IOException;
import java.net.SocketException;

import de.lange.server.manager.ReceiveManager;

/**
 * 
 * @author Franz Lange
 * 23.08.2015
 *
 */
public class Server {
	
	private ReceiveManager receiveManager;
	
	public Server(int port) throws SocketException{
		this.setReceiveManager(new ReceiveManager(port));
		
	}
	
	public Server(int port, int looserate) throws SocketException{
		this.setReceiveManager(new ReceiveManager(port, looserate));
		
	}
	
	public static void main(String[] args){
		try {
			int looserate = 0;
			
			System.out.println("[ INFO  ] Server started on port: " + args[0] + ".");
			
			if(args.length == 2)
				looserate = Integer.parseInt(args[1]);
			
			Server server = new Server(Integer.parseInt(args[0]), looserate);
			server.getReceiveManager().start();
			
			System.out.println("[ INFO  ] Server ready.");
			
			System.out.println("[ INFO  ] Press ENTER to stop.");
			System.in.read();
			
			server.getReceiveManager().interrupt();
			server.getReceiveManager().join();
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("[ INFO  ] Server stoped.");
		
		System.exit(0);
	}

	public ReceiveManager getReceiveManager() {
		return receiveManager;
	}

	public void setReceiveManager(ReceiveManager receiveManager) {
		this.receiveManager = receiveManager;
	}
}
