package de.lange.client.transmitter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.CRC32;

import de.lange.client.handler.ClientPacketHandler;
import de.lange.handler.PacketHandler;

/**
 * 
 * @author Franz Lange
 * 21.08.2015
 * 
 * Transmitter sends a file given by fileName
 * 
 */
public class Transmitter extends Thread {

	private DatagramSocket transmitterSocket;
	private File file;
	private FileInputStream fileInputStream;
	private InetAddress address;
	private int port;
	private ClientPacketHandler clientPacketHandler;
	volatile private double progress;
	private long startTime;
	private long endTime;
	private long fileSize;
	
	volatile private boolean interrupted;
	
	/**
	 * @param address
	 * @param port
	 * @param fileName
	 * @throws SocketException
	 * @throws FileNotFoundException
	 * @throws UnknownHostException
	 */
	public Transmitter(String address,int port,String fileName) throws SocketException, FileNotFoundException, UnknownHostException{
		this.transmitterSocket = new DatagramSocket();
		transmitterSocket.setSoTimeout(2000);
		
		this.address = InetAddress.getByName(address);
		this.port = port;
		this.file = new File(fileName);
		this.fileInputStream = new FileInputStream(file);
		this.interrupted = false;
		this.progress = 0;
		
		clientPacketHandler = new ClientPacketHandler();
	}
	
	/**
	 * 
	 */
	@Override
	public void run(){
		if(this.isInterrupted())
			return;
		startTime = Calendar.getInstance().getTimeInMillis();
		boolean correctResponse = false;
		
		for(int sendTry = 1; sendTry <= 10; sendTry++) {
			Map<String, Object> startMap = new LinkedHashMap<String, Object>();
			
			if(correctResponse)
				break;
			
			startMap.put(ClientPacketHandler.FILE_NAME,file.getName());
			startMap.put(ClientPacketHandler.FILE_SIZE,(int)file.length());
			
			fileSize = file.length();
			
			DatagramPacket startPacket = clientPacketHandler.getStartPacket(startMap);
			startPacket.setAddress(address);
			startPacket.setPort(port);
			
			try {
				transmitterSocket.send(startPacket);
				
				DatagramPacket startResponse = new DatagramPacket(new byte[2048], 2048);
				transmitterSocket.receive(startResponse);
				
				correctResponse = clientPacketHandler.valitatePacket(startResponse);
			} catch (SocketTimeoutException e) {
				System.err.println("[ ERROR ] Socket timeout.");
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(sendTry >= 10){
					System.err.println("[ ERROR ] Server not found.");
					progress = 1;
					return;
				}
			}
		}
		
		try {
			long fileSize = fileInputStream.getChannel().size();
//			System.out.println("[ DEBUG ] Filesize (long): " + fileSize + "\n\tFilesize (int): " + (int)fileSize);
			long currentFilePossition = 0;
			int numberOfBytes = 0;
			byte[] bytes = new byte[1024];
			
			while(numberOfBytes != -1 && !this.isInterrupted() && progress != 1.0){
				
				Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
				
				//read data from FileInputStream
				numberOfBytes = fileInputStream.read(bytes);
				if(numberOfBytes < 1024){
					byte[] fileArray = new byte[(int)file.length()];
					FileInputStream fis = new FileInputStream(file);
					fis.read(fileArray);
					fis.close();
					CRC32 crc = new CRC32();
					crc.update(fileArray);
					
//					System.out.println("[ DEBUG ] CRC32: " + (int)crc.getValue());
					
					dataMap.put(ClientPacketHandler.CHECKSUM, (int)crc.getValue());
				}

				dataMap.put(ClientPacketHandler.DATA, bytes);
				
				int sendTry = 0;
				
				do {
					sendTry++;
					// get DatagrammPacket from packetHandler 
					DatagramPacket datagramPacket = clientPacketHandler.getPacket(dataMap);
					datagramPacket.setAddress(address);
					datagramPacket.setPort(port);
					transmitterSocket.send(datagramPacket);		// and send it

					// receive response
					DatagramPacket responsePacket = new DatagramPacket(new byte[2048], 2048);
					try{
						transmitterSocket.receive(responsePacket);
						// validate response
						correctResponse = clientPacketHandler.valitatePacket(responsePacket);
					} catch (SocketTimeoutException e){
						System.err.println("[ ERROR ] Socket timeout.");
					}
				} while(!correctResponse && sendTry <= 10 && !this.isInterrupted());
				
				if(sendTry >= 10) {
					System.err.println("[ ERROR ] A problem occurred, connection lost.");
					return;
				}
				
				currentFilePossition += numberOfBytes;
				this.progress = (double)currentFilePossition/(double)fileSize;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		endTime = Calendar.getInstance().getTimeInMillis();
	}
	
	@Override
	public void interrupt(){
		this.interrupted = true;
	}
	
	@Override
	public boolean isInterrupted(){
		return this.interrupted;
	}

	public InetAddress getAddress() {
		return address;
	}

	public void setAddress(InetAddress address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public double getProgress() {
		return progress;
	}

	public void setProgress(double progress) {
		this.progress = progress;
	}
	
	public long getSendTime(){
		if(endTime == 0 || startTime == 0)
			return -1;
		return endTime - startTime;
	}
	
	public long getFileSize(){
		return fileSize;
	}
}
