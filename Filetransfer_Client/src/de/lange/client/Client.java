package de.lange.client;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;

import de.lange.client.transmitter.Transmitter;

/**
 * 
 * @author Franz Lange
 * 21.08.2015
 *
 */
public class Client {
	private Transmitter transmitter;
	
	public Client(String address,int port,String fileName) throws SocketException, FileNotFoundException, UnknownHostException {
		
		System.out.println("[ INFO  ] Target setver address: " + address);
		System.out.println("[ INFO  ] Target server port:    " + port);
		System.out.println("[ INFO  ] File name: " + fileName);
		this.transmitter = new Transmitter(address, port, fileName);
		
	}
	
	public static void main(String[] args) {
		String address = "";
		int port = 0;
		String fileName = "";
		
		if(args.length >= 3) {
			address = args[0];
			port = Integer.parseInt(args[1], 10);
			fileName = args[2];
		} else {
			try {
				System.out.print("[ INPUT ] Server Address: ");
				address = new BufferedReader(new InputStreamReader(System.in)).readLine();
				System.out.print("[ INPUT ] Server Port: ");
				port = Integer.parseInt(new BufferedReader(new InputStreamReader(System.in)).readLine());
				System.out.print("[ INPUT ] Send File: ");
				fileName = new BufferedReader(new InputStreamReader(System.in)).readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			Client client = new Client(address, port, fileName);
			
			
			client.transmitter.start();
			
			do {
				Thread.sleep(1000); // sleep 1sec
				System.out.println("[ INFO  ] Filetransferprogress: " + (int)(client.transmitter.getProgress()*100) + "%");
			} while(client.transmitter.getProgress()<1);
			
			System.out.println("[ INFO  ] Filetransfer ended.");
			System.out.println("[ INFO  ] Bit rate: " + ((client.transmitter.getFileSize() * 1000) / (client.transmitter.getSendTime() * 1024)) + " KB/s" );
			client.transmitter.join();
			
		} catch (SocketException | UnknownHostException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.err.println("[ ERROR ] File " + fileName + " not found!");
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
