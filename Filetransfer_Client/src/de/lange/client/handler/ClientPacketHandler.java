package de.lange.client.handler;

import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.prefs.BackingStoreException;
import java.util.zip.CRC32;

import de.lange.handler.PacketHandler;

/**
 * 
 * @author Franz Lange 14.11.2015
 *
 */
public class ClientPacketHandler implements PacketHandler {

	public static final String SESSION_NUMBER = "sessionNumber";
	public static final String PACKET_NUMBER = "packetNumber";
	public static final String DATA = "data";
	public static final String CHECKSUM = "checksum";
	public static final String FILE_NAME = "fileName";
	public static final String FILE_SIZE = "fileSize";
	public static final String REST = "rest";
	
	private short sessionNumber;
	private byte lastPacketNumber;
	
	public ClientPacketHandler() {
		this.sessionNumber = (short)new Random().nextInt();
		this.lastPacketNumber = -1;
		
		System.out.println("[ INFO  ] sessionNumber: " + sessionNumber);
	}
	
	@Override
	public boolean valitatePacket(DatagramPacket datagramPacket) {
		Map<String, Object> dataMap = getDataMap(datagramPacket);
		
		// validate sessionNumber
		if(!dataMap.containsKey(SESSION_NUMBER))
			throw new IllegalArgumentException("[ ERROR ] Invalid data Map.");
		if((short)dataMap.get(SESSION_NUMBER) != sessionNumber)
			return false;
		// validate packetNumber
		if(!dataMap.containsKey(PACKET_NUMBER))
			throw new IllegalArgumentException("[ ERROR ] Invalid data Map.");
		if((byte)dataMap.get(PACKET_NUMBER) != lastPacketNumber)
			return false;
		
		return true;
	}

	@Override
	public Map<String, Object> getDataMap(DatagramPacket datagramPacket) {
		Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
		
		ByteBuffer data = ByteBuffer.allocate(datagramPacket.getData().length);
		data.put(datagramPacket.getData());
		data.flip();
		
		dataMap.put(SESSION_NUMBER, data.getShort());
		dataMap.put(PACKET_NUMBER, data.get());
		
		if(datagramPacket.getLength() > 3){
			byte[] rest = new byte[datagramPacket.getLength()-3];
			data.get(rest);
			dataMap.put(REST, rest);
		}
		
		return dataMap;
	}

	@Override
	public DatagramPacket getPacket(Map<String, Object> dataMap) {
		if(!dataMap.containsKey(DATA))
			throw new IllegalArgumentException("[ ERROR ] Invalid data Map.");
		
		if(lastPacketNumber == -1)
			throw new IllegalStateException("[ ERROR ] You may not created a Startpacket!");
		if(lastPacketNumber == 0)
			lastPacketNumber = 1;
		else
			lastPacketNumber = 0;
		
		int dataLenght = 0;
		
		if(dataMap.containsKey(CHECKSUM))
			dataLenght = ((byte[])dataMap.get(DATA)).length + 7;
		else 
			dataLenght = ((byte[])dataMap.get(DATA)).length + 3;
		
		ByteBuffer data = ByteBuffer.allocate(dataLenght);
		
		data.putShort(sessionNumber);
		data.put(lastPacketNumber);
		data.put((byte[])dataMap.get(DATA));
		
		if(dataMap.containsKey(CHECKSUM))
			data.putInt((int)dataMap.get(CHECKSUM));
		
		data.flip();
		byte[] actualData = new byte[dataLenght];
		
		data.get(actualData);
		
		return new DatagramPacket(actualData, dataLenght);
	}
	
	public DatagramPacket getStartPacket(Map<String, Object> dataMap) {
		
		if(!dataMap.containsKey(FILE_NAME))
			throw new IllegalArgumentException("[ ERROR ] Invalid data Map.");
		if(!dataMap.containsKey(FILE_SIZE))
			throw new IllegalArgumentException("[ ERROR ] Invalid data Map.");
		
		ByteBuffer data = ByteBuffer.allocate(18+((String)dataMap.get("fileName")).length());
		data.putShort(sessionNumber);
		data.put((byte)0);
		data.put("Start".getBytes());
		data.putInt((int)dataMap.get(FILE_SIZE));
		data.putShort((short)((String)dataMap.get(FILE_NAME)).length());
		data.put(((String)dataMap.get(FILE_NAME)).getBytes());
		
		data.position(0);
		byte[] actualData = new byte[14+((String)dataMap.get(FILE_NAME)).length()];
		data.get(actualData);
		CRC32 crc = new CRC32();
		crc.update(actualData);
		
		data.putInt((int)crc.getValue());
		
		data.flip();
		byte[] startPacket = new byte[18+((String)dataMap.get(FILE_NAME)).length()];
		data.get(startPacket);
		
		lastPacketNumber = 0;
		
		return new DatagramPacket(startPacket, startPacket.length);
	}

}
