package de.lange.handler;

import java.net.DatagramPacket;
import java.util.Map;

/**
 * 
 * @author Franz Lange 07.11.2015
 *
 */
public interface PacketHandler {
	
	/**
	 * Validates a {@link DatagramPacket} to a specific protocol.
	 * 
	 * @param datagramPacket
	 * @return true if the packet is valid to the protocol
	 */
	public boolean valitatePacket(DatagramPacket datagramPacket);
	
	/**
	 * Builds a dataMap form a given {@link DatagramPacket} using a specific protocol.
	 * 
	 * @param datagramPacket
	 * @return the dataMap
	 */
	public Map<String, Object> getDataMap(DatagramPacket datagramPacket);
	
	/**
	 * Builds a {@link DatagramPacket} from a given dataMap using a specific protocol.
	 * @param dataMap
	 * @return the generated {@link DatagramPacket}
	 */
	public DatagramPacket getPacket(Map<String, Object> dataMap);
	
}
